<?php

namespace Drupal\soembed\Plugin\Filter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\media\IFrameUrlHelper;
use Drupal\media\OEmbed\ProviderRepositoryInterface;
use Drupal\media\OEmbed\Resource;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\OEmbed\ResourceFetcherInterface;
use Drupal\media\OEmbed\UrlResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to embed media via oEmbed.
 *
 * @Filter(
 *   id = "filter_soembed",
 *   title = @Translation("Simple oEmbed filter"),
 *   description = @Translation("Embeds media for URL that supports oEmbed standard."),
 *   settings = {
 *     "soembed_maxwidth" = 0,
 *     "soembed_replace_inline" = FALSE,
 *     "soembed_allowed_buckets" = {}
 *   },
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class SoEmbedFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The media settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The oEmbed resource fetcher.
   *
   * @var \Drupal\media\OEmbed\ResourceFetcherInterface
   */
  protected $resourceFetcher;

  /**
   * The oEmbed URL resolver service.
   *
   * @var \Drupal\media\OEmbed\UrlResolverInterface
   */
  protected $urlResolver;

  /**
   * The iFrame URL helper service.
   *
   * @var \Drupal\media\IFrameUrlHelper
   */
  protected $iFrameUrlHelper;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The providerRepository.
   *
   * @var \Drupal\media\OEmbed\ProviderRepositoryInterface
   */
  protected $providerRepository;

  /**
   * An array of this filters enabled oembed providers.
   *
   * @var array
   */
  protected $enabledProviders;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new OEmbed instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\media\OEmbed\ProviderRepositoryInterface $provider_repository
   *   The oEmbed provider repository service.
   * @param \Drupal\media\OEmbed\ResourceFetcherInterface $resource_fetcher
   *   The oEmbed resource fetcher service.
   * @param \Drupal\media\OEmbed\UrlResolverInterface $url_resolver
   *   The oEmbed URL resolver service.
   * @param \Drupal\media\IFrameUrlHelper $iframe_url_helper
   *   The iFrame URL helper service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, ProviderRepositoryInterface $provider_repository, ResourceFetcherInterface $resource_fetcher, UrlResolverInterface $url_resolver, IFrameUrlHelper $iframe_url_helper, RendererInterface $renderer, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config_factory->get('media.settings');
    $this->logger = $logger_factory->get('media');
    $this->providerRepository = $provider_repository;
    $this->resourceFetcher = $resource_fetcher;
    $this->urlResolver = $url_resolver;
    $this->iFrameUrlHelper = $iframe_url_helper;
    $this->renderer = $renderer;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('media.oembed.provider_repository'),
      $container->get('media.oembed.resource_fetcher'),
      $container->get('media.oembed.url_resolver'),
      $container->get('media.oembed.iframe_url_helper'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $lines = explode("\n", $text);

    if (!empty($this->settings['soembed_replace_inline'])) {
      $lines = preg_replace_callback('#(?<!["\'=])(https?://[^\s<]+)(?![^<>]*?>)#', [$this, 'embed'], $lines);
    }
    else {
      $lines = preg_replace_callback('#^(<p>)?(https?://\S+?)(</p>)?$#', [$this, 'embed'], $lines);
    }

    $text = implode("\n", $lines);

    return new FilterProcessResult($text);
  }

  /**
   * Get the enabled providers for the filter.
   *
   * @todo Should this be cached?
   */
  private function getEnabledProviders() {
    if (is_null($this->enabledProviders)) {
      $this->enabledProviders = [];
      // If the oembed_provider module is not enabled return an empty array.
      if ($this->moduleHandler->moduleExists('oembed_providers')) {
        // Load the filters selected buckets.
        $buckets = $this->entityTypeManager->getStorage('oembed_provider_bucket')
          ->loadMultiple($this->settings['soembed_allowed_buckets']);
        // Add each buckets oembed providers to our list.
        foreach ($buckets as $bucket) {
          $this->enabledProviders += $bucket->get('providers');
        }
      }
    }

    return $this->enabledProviders;
  }

  /**
   * Get the provider by url.
   */
  private function getProviderByUrl($url) {
    // Check the URL against every scheme of every endpoint of every provider
    // until we find a match.
    foreach ($this->providerRepository->getAll() as $provider_info) {
      foreach ($provider_info->getEndpoints() as $endpoint) {
        if ($endpoint->matchUrl($url)) {
          return $provider_info;
        }
      }
    }

    return NULL;
  }

  /**
   * Turn URL into iframe via oEmbed.
   *
   * Referred from Drupal\media\Plugin\Field\FieldFormatter::viewElements().
   */
  private function embed($match) {
    if (!empty($this->settings['soembed_replace_inline'])) {
      $value = $match[1];
    }
    else {
      $value = $match[2];
    }

    $max_width = $this->settings['soembed_maxwidth'];
    $max_height = 0;

    if (!$provider = $this->getProviderByUrl($value)) {
      // No provider for this url.
      return $value;
    }

    try {
      if ($this->getEnabledProviders()) {
        // Check that this urls provider is in one of the enabled buckets.
        if (in_array($provider->getName(), $this->getEnabledProviders())) {
          $resource_url = $this->urlResolver->getResourceUrl($value, $max_width, $max_height);
          $resource = $this->resourceFetcher->fetchResource($resource_url);
        }
        else {
          return $match[0];
        }
      }
      else {
        // No providers are enabled by buckets. Get the oembed resource if
        // there is one.
        $resource_url = $this->urlResolver->getResourceUrl($value, $max_width, $max_height);
        $resource = $this->resourceFetcher->fetchResource($resource_url);
      }
    }
    catch (ResourceException $exception) {
      $this->logger->error("Could not retrieve the remote URL (@url).", ['@url' => $value]);
      return $match[0];
    }
    catch (\Exception $exception) {
      return $match[0];
    }

    if ($resource->getType() === Resource::TYPE_LINK) {
      $build = [
        '#title' => $resource->getTitle(),
        '#type' => 'link',
        '#url' => Url::fromUri($value),
      ];
    }
    elseif ($resource->getType() === Resource::TYPE_PHOTO) {
      $build = [
        '#theme' => 'image',
        '#uri' => $resource->getUrl()->toString(),
        '#width' => $max_width ?: $resource->getWidth(),
        '#height' => $max_height ?: $resource->getHeight(),
      ];
    }
    else {
      $url = Url::fromRoute('media.oembed_iframe', [], [
        'query' => [
          'url' => $value,
          'max_width' => $max_width,
          'max_height' => $max_height,
          'hash' => $this->iFrameUrlHelper->getHash($value, $max_width, $max_height),
        ],
      ]);

      $domain = $this->config->get('iframe_domain');
      if ($domain) {
        $url->setOption('base_url', $domain);
      }

      // Render videos and rich content in an iframe for security reasons.
      // @see: https://oembed.com/#section3
      $build = [
        '#type' => 'html_tag',
        '#tag' => 'iframe',
        '#attributes' => [
          'src' => $url->toString(),
          'frameborder' => 0,
          'scrolling' => FALSE,
          'allowtransparency' => TRUE,
          'width' => $max_width ?: $resource->getWidth(),
          'height' => $max_height ?: $resource->getHeight(),
          'class' => ['media-oembed-content'],
        ],
        '#attached' => [
          'library' => [
            'media/oembed.formatter',
          ],
        ],
      ];

      // An empty title attribute will disable title inheritance, so only
      // add it if the resource has a title.
      $title = $resource->getTitle();
      if ($title) {
        $build['#attributes']['title'] = $title;
      }

      CacheableMetadata::createFromObject($resource)
        ->addCacheTags($this->config->getCacheTags())
        ->applyTo($build);
    }

    if (!empty($this->settings['soembed_replace_inline'])) {
      return $this->renderer->render($build);
    }
    else {
      return ($match[1] ?? '') . $this->renderer->render($build) . ($match[3] ?? '');
    }
  }

  /**
   * Define settings for text filter.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['soembed_maxwidth'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum width of media embed'),
      '#default_value' => $this->settings['soembed_maxwidth'],
      '#description' => $this->t('Leave to zero to use original values.'),
    ];
    $form['soembed_replace_inline'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace in-line URLs'),
      '#default_value' => $this->settings['soembed_replace_inline'],
      '#description' => $this->t('Recognize in-line URLs. Enable too if the wysiwyg editor saves the content source in one line.'),
    ];

    if ($this->moduleHandler->moduleExists('oembed_providers')) {
      $options = [];
      if ($buckets = $this->entityTypeManager->getStorage('oembed_provider_bucket')->loadMultiple()) {
        foreach ($buckets as $bucket) {
          $options[$bucket->id()] = $bucket->label();
        }
      }
      $form['soembed_allowed_buckets'] = [
        '#title' => $this->t('Oembed providers buckets'),
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $this->settings['soembed_allowed_buckets'],
        '#description' => $this->t('Select all the buckets with oembed providers to enable for this filter.'),
        '#element_validate' => [[static::class, 'validateOptions']],
      ];
    }
    else {
      $form['soembed_allowed_buckets'] = [
        '#type' => 'value',
        '#value' => [],
      ];
      $form['soembed_allowed_buckets_warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            $this->t('Install the <a href="https://www.drupal.org/project/oembed_providers" target="_blank">oembed_providers</a> module to control which providers are used in this filter. Otherwise all possible providers will be enabled.'),
          ],
        ],
        '#weight' => -100,
      ];
    }

    return $form;
  }

  /**
   * Form element validation handler.
   *
   * @param array $element
   *   The allowed_view_modes form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateOptions(array &$element, FormStateInterface $form_state) {
    // Filters the #value property so only selected values appear in the
    // config.
    $form_state->setValueForElement($element, array_filter($element['#value']));
  }

}
