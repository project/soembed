
INTRODUCTION
------------

Simplify adding videos and other rich media by inserting URL using oEmbed
technology (https://oembed.com).

You can put oEmbed supported URLs in content body on its own line and it will
automatically turn into rich media. If not recognized, the URL will display as
is. Version 2.x now uses Drupal 9's core oEmbed support in Media module.


INSTALLATION
------------

 - Install the Simple oEmbed module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for more info.

 - Configure text format at /admin/config/content/formats.

 - Check Simple oEmbed filter.

 - Reorder so that Simple oEmbed filter comes before Convert URLs into links.
   Any filter that operates on URL before Simple oEmbed filter can disrupt
   desired result.

 - Set maximum width of the embedded media (default 0).

 - If you need to be able to embed a video inline (e.g. in the middle of a
   paragraph), check the "Replace in-line URLs" box.


REQUIREMENTS
------------

This module requires Drupal core's Media module.
